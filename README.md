# Necro Trans

Necro Trans is a set of transformers for Skel files. It provides different ways to generate skeletal semantics from skeletal semantics.
It can be installed via opam, and depends on [Necro Lib](https://gitlab.inria.fr/skeletons/necro).

## `flatten_modules`

This transformer takes a file and its dependencies, and returns a unique file,
with no sub-module, semantically equivalent to the initial file.
