(**************************************************************************)
(*                                                                        *)
(*                           Necro Transformers                           *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

(** This module provides a list of transformers for the skeletal semantics.
    Though this module produces skeletal semantics that do not come from an
    actual file, the semantics is supposed to be well-typed. *)

open Necro
open TypedAST

(*
val simplelet: skeletal_semantics -> skeletal_semantics
(** Delete let matching on constructor and replace them by calls to an
    unspecified term acting as destructor. *)

val delete_monads: skeletal_semantics -> skeletal_semantics
(** Delete all monadic [let%bind] construct, and replace them by explicit
    application of [bind]. *)

val explode: skeletal_semantics -> skeletal_semantics
(** Explode all branchings, so that each specified term contains at most one
    branching, at the head (the resulting terms may also contain branching
    inside of a function definition). *)

val extract_letin: skeletal_semantics -> skeletal_semantics
(** Extract inner letin, giving fresh names if need be. *)

val inline_monads: skeletal_semantics -> skeletal_semantics
(** Inline all [let%bind] construct *)

val eta_expanse: skeletal_semantics -> skeletal_semantics
(** Perform an η-expansion on all declared terms, that is if a term is of type
    `val _: a → b → c = f`, write it under the form λ x: a → λ y: b → f a b`*)

val inline_term: string -> skeletal_semantics -> skeletal_semantics
(** Inline all occurences of a term *)

val remove_unused: skeletal_semantics -> skeletal_semantics
(** replace unused variables by underscore *)
*)


(*

(*********************************************************)
(*                    Module inlining                    *)
(*********************************************************)

module PMap : Map.S with type key = Path.Abs.t * String.t

type remapping =
	{ constructors : string PMap.t
	; fields : string PMap.t
	; types : string PMap.t
	; terms : string PMap.t
	; binders : string PMap.t
	}

val flatten_modules:
	string list ->
	(string * skeletal_semantics) list ->
	(string * skeletal_semantics) ->
		skeletal_semantics * remapping
(** Return an equivalent skeletal semantics with only one module, along with
		the remappings *)

*)


(*********************************************************)
(*                    Remove shadowing                   *)
(*********************************************************)

(** Semantics transformers *)
module Semantics : sig
  type dependency = (string * skeletal_semantics)

  val lift: (term -> term) -> dependency list -> dependency ->
    skeletal_semantics
end


(** Term transformers *)
module Term : sig
  val remove_redef: term -> term
  (** Alpha-rename some pattern variables to remove shadowing *)

  val make_anf: term -> term
  (** push nested letins / branches / matches up so that the bound skeleton of a
   match or a let-in is always either a [Return], an [Apply], or an [Exists]. *)

  val remove_or_patterns: term -> term
  (** Rewrite into an equivalent semantics without any or-pattern *)
end

