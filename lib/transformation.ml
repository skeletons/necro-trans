(**************************************************************************)
(*                                                                        *)
(*                           Necro Transformers                           *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

open Necro
open TypedAST
open Util

(*open Util
open Skeltypes
open Interpretation
open Skeleton

(*****************************************************************************)
(*                                 simplelet                                 *)
(*****************************************************************************)

let print_from from = Option.fold ~none:"" ~some:(fun x -> x ^ "::") from

(* Change a type to reflect its origin *)
let rec type_from from =
	begin function
	| Variable v -> Variable v
	| UserType (from', n, b, ta) ->
			let new_from = Option.fold ~none:from ~some:Option.some from' in
			UserType (new_from, n, b, List.map (type_from from) ta)
	| Product l ->
			Product (List.map (type_from from) l)
	| Arrow (i, o) ->
			Arrow (type_from from i, type_from from o)
	end


(* An interpretation for simplelet. Given a term/skeleton, the resulting
 * functions return the map of constructors that have been destructed to
 * their type and the new term/skeleton *)
let simplelet_interpretation destr :
	(necro_type SMap.t * term, necro_type SMap.t * skeleton) interpretation =
		let merge_maps =
			List.fold_left (SMap.union (fun _ _ x -> Some x)) SMap.empty
		in
		let destr' c c_ty p ta =
			TVar (TVTerm (Unspecified, None, destr c, ta, Arrow (c_ty, pattern_typ p)))
		in
		let letin map a p s1 s2 =
			let av = all_variables_skel s2 in
			let rec is_tuple_type p =
				begin match p with
				| PWild _ | PVar _ -> true
				| PConstr _ | PRecord _ -> false
				| PTuple p -> List.for_all is_tuple_type p
				end
			in
			begin match p with
			| _ when is_tuple_type p -> map, LetIn (a, p, s1, s2)
			| PConstr ((_src,c,c_ty), ta, p') ->
					let x = fresh (fun x -> not (av x)) "x" in
					let tv_x = TVLet (x, c_ty) in
					let tv_x_term = TVar tv_x in
					SMap.add c c_ty map,
					LetIn (a, PVar (x, c_ty), s1,
						LetIn (None, p', Apply (destr' c c_ty p' ta, [tv_x_term]), s2))
			| PTuple ps ->
					let _, xs = List.fold_left (fun (f, l) _ ->
						let new_x = fresh (fun x -> not (f x)) "x" in
						((fun x -> f x || x = new_x),new_x :: l)) (av, []) ps
					in
					let almost = List.fold_left2 (fun s xn pn ->
						LetIn (None, pn, Return (TVar (TVLet (xn, pattern_typ pn))) , s)
						) s2 xs ps
					in
					let xs' = List.map2 (fun x p -> PVar (x, pattern_typ p)) xs ps in
					map, LetIn (a, PTuple xs', s1, almost)
			| PRecord ps ->
					let new_x = fresh (fun x -> not (av x)) "x" in
					let pat_x = PVar (new_x, pattern_typ p) in
					let var_x = TVar (TVLet (new_x, pattern_typ p))
					in
					let almost = List.fold_left (fun s ((f, c, ty), ta, pn) ->
					LetIn (None, pn, Return (TField (var_x, ta, (f,c,ty))),
						s)
						) s2 ps in
					map, LetIn (a, pat_x, s1, almost)
			| _ -> raise InternalError (* Can't happen *)
			end
		in
	{
		var_interp = (fun v ->
			SMap.empty, TVar v) ;
		constr_interp = (fun c ta (m, t) ->
			m, TConstr (c, ta, t)) ;
		tuple_interp = (fun ct ->
			let c, t = List.split ct in merge_maps c, TTuple t) ;
		function_interp = (fun p (c, s) ->
			c, TFunc (p, s)) ;
		field_interp = (fun (m, t) ta f ->
			m, TField (t, ta, f)) ;
		record_interp = (fun l ->
			let l = List.map (fun (x, y, (m, z)) ->
				(x, y, z), m) l in
			let l, m = List.split l in
			merge_maps m, TRecMake l) ;
		recset_interp = (fun (m1, v') l ->
			let l = List.map (fun (x, y, (m, z)) ->
				(x, y, z), m) l in
			let l, m = List.split l in
			merge_maps (m1 :: m), TRecSet (v', l)) ;
		letin_interp = (fun a p (c1, s1) (c2, s2) ->
			let c = SMap.union (fun _ _ x -> Some x) c1 c2 in
			letin c a p s1 s2);
		exists_interp = (fun p ty (m,s) ->
			(m, Exists (p, ty, s))) ; (* TODO: maybe simplify exists too *)
		apply_interp = (fun (c, t) ctl ->
			let cl, tl = List.split ctl in
			merge_maps (c :: cl), Apply (t, tl)) ;
		merge_interp = (fun s cb ->
			let c, b = List.split cb in
			merge_maps c, Branching (s, b)) ;
		match_interp = (fun (c, s) ty pcsl ->
			let pl, csl = List.split pcsl in
			let cl, sl = List.split csl in
			let psl = List.combine pl sl in
			merge_maps (c :: cl), Match (s, ty, psl)) ;
		return_interp = (fun (c, t) ->
			c, Return t)
	}

let simplelet ss =
	let destr c =
		let used h = SMap.mem h ss.ss_terms in
		let std_name = "_destr_" ^ c in
		let ok h = not (used h) in
		fresh ok std_name
	in
	let simplelet_aux ss =
		let open Interpretation in
		let simplelet_term t =
			interpret_term (simplelet_interpretation destr) t
		in
		let simplelet_term_decl (com, ta, ty, term) =
			begin match term with
			| None -> (SMap.empty, (com, ta, ty, None))
			| Some t ->
					let (c', t') = simplelet_term t in
					(c', (com, ta, ty, Some t'))
			end
		in
		let simplelet_ss_terms s =
			let union m1 m2 = SMap.union (fun _ _ y -> Some y) m1 m2 in
			SMap.fold (fun s term (constr, map) ->
				let constr', term' = simplelet_term_decl term in
				(union constr constr', SMap.add s term' map))
			s (SMap.empty, SMap.empty)
		in
		let constr, ss_terms = simplelet_ss_terms ss.ss_terms in
		let destructor c s =
			let from, s =
				begin match unalias ss s with
				| UserType (from, s, Variant, _) -> from, s
				| _ -> raise InternalError
				end
			in
			let rec aux from s ss =
				begin match from with
				| None -> SMap.find s ss.ss_types
				| Some o -> aux None s (fst (SMap.find o ss.ss_includes))
				end
			in
			let cl =
				begin match aux from s ss with
				| (_, TDVariant (_ta, cl)) -> cl
				| _ -> raise InternalError
				end
			in
			let csig = List.find (fun cs -> cs.cs_name = c) cl in
			let destr_sig =
				let output_type =
					UserType (None, csig.cs_output_type, Variant, List.map (fun x -> Variable x) csig.cs_type_args) in
				Arrow ( output_type,
								type_from from csig.cs_input_type )
			in
			let comm =
				Some (" Destructor for " ^ print_from from ^ c ^ " ")
			in
			(comm, csig.cs_type_args, destr_sig, None)
		in
		let add_destructors ss_terms =
			SMap.fold (fun c s (m, accu) ->
				let name = destr c in
				(SMap.add name (destructor c s) m, name :: accu))
			constr (ss_terms, [])
		in
		let ss_terms, destructors = add_destructors ss_terms in
		{ ss with ss_terms }, destructors
	in
	let e = ref ss in
	let flag = ref false in
	let new_destrs = ref [] in
	let () =
		while not !flag do
			(* TODO: instead of comparing e and new_e, have simplelet_aux return the
				 flag saying if it was modified *)
			let new_e, new_new_destrs = simplelet_aux !e in
			let () = flag := same_ss new_e !e in
			let () = new_destrs := new_new_destrs @ !new_destrs in
			let () = e := new_e in
			()
		done
	in
	let ss_order =
		let destructors = List.sort_uniq String.compare !new_destrs in
		ss.ss_order @ (List.map (fun x -> DTerm x) destructors)
	in
	{!e with ss_order}



(*****************************************************************************)
(*                               delete monads                               *)
(*****************************************************************************)

let delete_monads_interp ss tmp =
	{ id_interpretation with
		letin_interp = (fun a p s1 s2 ->
			begin match a with
			| None -> LetIn (a, p, s1, s2)
			| Some (k, _src, b, ta, _) ->
					(* let tmp1 = s1 in
					 * b<ta> tmp1 (λ tmp2 -> let p = tmp2 in s2) *)
					let a = pattern_typ p in
					let ma = skeleton_type ss s1 in
					let mb = skeleton_type ss s2 in
					let tmp2_ty = Arrow(Arrow(a, mb), mb) in
					let b_ty = Arrow(ma, tmp2_ty) in
					let tmp1 = TVLet (tmp, ma) in
					let tmp2 = TVLet (tmp, pattern_typ p) in
					LetIn (None, PVar (tmp, ma), s1,
						Apply(
								TVar (TVTerm (k, None, b, ta, b_ty)),
							(TVar tmp1) ::
							TFunc (PVar (tmp, tv_typ tmp2),
								LetIn (None, p,
									Return (TVar tmp2),
									s2)) ::
							[]
						)
					)
			end);
	}

let delete_monads ss =
	let ok x = not (all_variables_ss ss x) && not (Necro.is_keyword x) in
	let tmp = fresh ok "_tmp" in
	let ss_terms =
		SMap.mapi (fun _name (com, ta, ty, t) ->
			(com, ta, ty, Option.map (interpret_term (delete_monads_interp ss tmp)) t)
		) ss.ss_terms
	in
	{ss with ss_terms}



(*****************************************************************************)
(*                                  explode                                  *)
(*****************************************************************************)

let exploding_interpretation ss =
	let rec typ_of_arrow f t =
		begin match unalias ss f, t with
		| _ty, [] -> f
		| Arrow (_, ty'), _ :: q -> typ_of_arrow ty' q
		| _ -> raise InternalError
		end
	in
	{
		var_interp = (fun v -> TVar v) ;
		constr_interp = (fun c ta t -> TConstr (c, ta, t)) ;
		tuple_interp = (fun t -> TTuple t) ;
		function_interp = (fun p (tys, sl) ->
			begin match sl with
			| s :: [] -> TFunc (p, s)
			| _ -> TFunc (p, Branching (tys, sl))
			end) ;
		field_interp = (fun t ta f -> TField (t, ta, f)) ;
		record_interp = (fun l -> TRecMake l) ;
		recset_interp = (fun o l -> TRecSet (o, l)) ;
		letin_interp = (fun a p (_ ,sl1) (ty, sl2) ->
			ty, List.map (fun (s1, s2) ->
				LetIn(a, p, s1, s2)) (list_square sl1 sl2)) ;
		exists_interp = (fun p ty (ty_out, sl) ->
			ty_out, List.map (fun s ->
				Exists(p, ty, s)) sl) ;
		apply_interp = (fun f t ->
			typ_of_arrow (term_type ss f) t, Apply (f, t) :: []) ;
		merge_interp = (fun ty l ->
			let l = List.flatten (List.map snd l) in
			(* Flattening all branchings *)
			let l =
				let rec flatten acc =
					begin function
					| [] -> acc
					| s :: l ->
							begin match s with
							| Branching (_, l') ->
									flatten acc (l' @ l)
							| _ -> flatten (s :: acc) l
							end
					end
				in
				flatten [] l
			in
			ty, List.sort_uniq compare l );
		match_interp = (fun t ty psll ->
			let psl = List.map (fun (p, (ty, sl)) ->
				(p, Branching (ty, sl))) psll in
			ty, [Match (t, ty, psl)]) ;
		return_interp = (fun t -> term_type ss t, Return t :: [])
	}


let explode ss =
	let explode_term = interpret_term (exploding_interpretation ss) in
	let ss_terms = SMap.map (fun (com, ta, ty, t) ->
		let t = Option.map explode_term t in
		(com, ta, ty, t)) ss.ss_terms
	in
	{ss with ss_terms}



(*****************************************************************************)
(*                     rename redefinition of variables                      *)
(*****************************************************************************)

let remove_redef_interp =
	let rec remove_redef_pattern p map t_all t_now =
		begin match p with
		| PWild _ -> (p, map, t_all, t_now)
		| PVar (name, typ) ->
				begin match SSet.mem name t_now with
				| false ->
						(p, map, SSet.add name t_all, SSet.add name t_now)
				| true ->
						let ok x = not (SSet.mem x t_all) in
						let new_name = fresh ok name in
						(PVar (new_name, typ),
							SMap.add name new_name map,
							SSet.add new_name t_all,
							SSet.add new_name t_now)
				end
		| PConstr (c, ta, p) ->
				let (p, map, t_all, t_now) = remove_redef_pattern p map t_all t_now in
				(PConstr (c, ta, p), map, t_all, t_now)
		| PTuple [] ->
				(p, map, t_all, t_now)
		(* FIXME: is there a nicer way to do that? *)
		| PTuple (p :: pq) ->
				let (p, map, t_all, t_now) = remove_redef_pattern p map t_all t_now in
				let (pq, map, t_all, t_now) = remove_redef_pattern (PTuple pq) map t_all t_now in
				begin match pq with
				| PTuple pq -> (PTuple (p :: pq), map, t_all, t_now)
				| _ -> raise InternalError
				end
		| PRecord [] ->
				(p, map, t_all, t_now)
		(* FIXME: is there a nicer way to do that? *)
		| PRecord ((c, ta, p) :: pq) ->
				let (p, map, t_all, t_now) = remove_redef_pattern p map t_all t_now in
				let (pq, map, t_all, t_now) = remove_redef_pattern (PRecord pq) map t_all t_now in
				begin match pq with
				| PRecord pq -> (PRecord ((c, ta, p) :: pq), map, t_all, t_now)
				| _ -> raise InternalError
				end
		end
	in
	let app t map taken_all taken_now = t map taken_all taken_now in
	let rename v name =
		begin match v with
		| TVLet (_, ty) -> TVLet (name, ty)
		| TVTerm _ -> v
		end
	in
	{
		var_interp = (fun v map _taken_all _taken_now ->
			begin match SMap.find_opt (tv_name v) map with
			| Some v' -> TVar (rename v v')
			| None -> TVar v
			end) ;
		constr_interp = (fun c ta t map taken_all taken_now ->
			TConstr (c, ta, app t map taken_all taken_now)) ;
		tuple_interp = (fun tl map taken_all taken_now ->
			TTuple (List.map (fun t -> app t map taken_all taken_now) tl) );
		function_interp = (fun p sk map taken_all taken_now ->
			let (p, map, taken_all, taken_now) =
				remove_redef_pattern p map taken_all taken_now
			in
			TFunc (p, app sk map taken_all taken_now) ) ;
		field_interp = (fun t ta f map taken_all taken_now ->
			TField (app t map taken_all taken_now, ta, f)) ;
		record_interp = (fun l map taken_all taken_now ->
			let do_one (x, y, z) = (x, y, app z map taken_all taken_now) in
			let l = List.map do_one l in
			TRecMake l) ;
		recset_interp = (fun o l map taken_all taken_now ->
			let do_one (x, y, z) = (x, y, app z map taken_all taken_now) in
			let o = app o map taken_all taken_now in
			let l = List.map do_one l in
			TRecSet (o, l)) ;
		letin_interp = (fun bind p s1 s2 map taken_all taken_now ->
			let (p', map', taken_all', taken_now') =
				remove_redef_pattern p map taken_all taken_now
			in
			LetIn(bind, p', app s1 map taken_all' taken_now',
			app s2 map' taken_all' taken_now')) ;
		exists_interp = (fun p ty s map taken_all taken_now ->
			let (p, map, taken_all, taken_now) =
				remove_redef_pattern p map taken_all taken_now
			in
			Exists(p, ty, app s map taken_all taken_now)) ;
		apply_interp = (fun t tl map taken_all taken_now ->
			let t = t map taken_all taken_now in
			Apply (t, List.map (fun t -> app t map taken_all taken_now) tl)) ;
		merge_interp = (fun ty sl map taken_all taken_now ->
			Branching (ty, List.map (fun t -> app t map taken_all taken_now) sl)) ;
		match_interp = (fun s ty psl map taken_all taken_now ->
			let s = s map taken_all taken_now in
			let fix_redef (p, s) =
				let (p, map, taken_all', taken_now') =
					remove_redef_pattern p map taken_all taken_now in
				(p, s map taken_all' taken_now')
			in
			let psl = List.map fix_redef psl in
			Match (s, ty, psl)
		) ;
		return_interp = (fun t map taken_all taken_now ->
			Return (app t map taken_all taken_now))
	}

let remove_redef ss =
	let remove_redef_in_term = interpret_term remove_redef_interp in
	let ss_terms = SMap.map (fun (com, ta, ty, t) ->
		begin match t with
		| None -> (com, ta, ty, None)
		| Some t ->
				let map = SMap.empty in
				let taken_all =
					SSet.union (all_variables_set_term t)
					(SMap.fold (fun k _ s -> SSet.add k s) ss.ss_terms SSet.empty)
				in
				let taken_now = SSet.empty in
				(com, ta, ty, Some (remove_redef_in_term t map taken_all taken_now))
		end) ss.ss_terms
	in
	{ ss with ss_terms }



(*****************************************************************************)
(*                            extract inner letin                            *)
(*****************************************************************************)

let extract_letin_interp =
	{ id_interpretation with
		letin_interp = (fun a p s1 s2 ->
			begin match s1 with
			| LetIn (a', p', s1', s2')->
					(* let %a p =
					 * 	let%a' p' = s1' in s2'
					 * in s2 *)
					begin match a' with
					| None ->
							LetIn (None, p', s1', LetIn(a, p, s2', s2))
					| Some _ ->
							LetIn (a, p, s1, s2) (* cannot extract an inner monadic letin *)
					end
			| _ -> LetIn (a, p, s1, s2)
			end)
	}

let extract_letin ss =
	let ss = remove_redef ss in
	let extract_in_term = interpret_term extract_letin_interp in
	let ss_terms = SMap.map (fun (com, ta, ty, t) ->
		(com, ta, ty, Option.map extract_in_term t)) ss.ss_terms
	in
	{ss with ss_terms}


(*****************************************************************************)
(*                         inline binding operators                          *)
(*****************************************************************************)


(* subst_sk f (Some arg) ty body sk is the beta-reduction of
   [ sk { f := (λ arg : ty → body) } ] *)
let rec subst_sk ss f arg ty_f_in body sk =
	let aux_sk sk' = subst_sk ss f arg ty_f_in body sk' in
	let aux_term v = subst_term ss f arg ty_f_in body v in
	begin match sk with
	| Branching (ty, sl) ->
			Branching (ty, List.map aux_sk sl)
	| Match (t, ty, psl) ->
			let do_one (p, s) =
				if List.mem f (pattern_variables p)
				then (p, s)
				else (p, aux_sk s)
			in
			Match (aux_term t, ty, List.map do_one psl)
	| LetIn (a, p, s1, s2) when List.mem f (pattern_variables p) ->
			LetIn (a, p, aux_sk s1, s2)
	| LetIn (a, p, s1, s2) ->
			LetIn (a, p, aux_sk s1, aux_sk s2)
	| Exists (p, ty, s) when List.mem f (pattern_variables p) ->
			Exists (p, ty, s)
	| Exists (p, ty, s) ->
			Exists (p, ty, aux_sk s)
	| Return tv -> Return (aux_term tv)
	| Apply (TVar TVLet (name, _), v1 :: vq)
			when name = f ->
				begin match arg with
				| None ->
						apply ss body vq
				| Some v ->
						apply ss (alpha_map_skel v v1 body) vq
				end
	| Apply (v, vl) ->
		Apply (aux_term v, List.map aux_term vl)
	end
and subst_term ss f arg ty_f_in body v =
	let aux_sk sk' = subst_sk ss f arg ty_f_in body sk' in
	let aux_term v = subst_term ss f arg ty_f_in body v in
	begin match v with
	| TVar (TVLet (name, ty)) when name = f ->
			begin match arg with
			| Some x ->
					let () =
						if not (type_equals ss ty ty_f_in) then raise InternalError in
					TFunc (PVar (x, ty_f_in), body)
			| None ->
					let () =
						if not (type_equals ss ty ty_f_in) then raise InternalError in
					TFunc (PVar ("_tmp", ty_f_in), body)
			end
	| TVar _ -> v
	| TFunc (PVar (x, _ty), _sk) when x = f -> v
	| TFunc (p, sk) ->
			TFunc (p, aux_sk sk)
	| TConstr (c, ta, v) -> TConstr (c, ta, aux_term v)
	| TTuple tl -> TTuple (List.map aux_term tl)
	| TField (v, ta, fd) -> TField (aux_term v, ta, fd)
	| TRecMake l ->
			let do_one (c, ta, v) = (c, ta, aux_term v) in
			TRecMake (List.map do_one l)
	| TRecSet (vo, l) ->
			let do_one (c, ta, v) = (c, ta, aux_term v) in
			TRecSet (aux_term vo, List.map do_one l)
	end



let rec inline_monads_term ss l v = (* TODO: use interpretation? *)
	let aux x l = inline_monads_term ss l x in
	let aux' x l = inline_monads_skel ss l x in
	begin match v with
	| TVar v -> TVar v
	| TConstr (c, ta, t) -> TConstr (c, ta, aux t l)
	| TTuple t ->
			let t = List.map (fun x -> aux x l) t in
			TTuple t
	| TFunc (p, s) ->
			TFunc (p, aux' s l)
	| TField (t, ta, f) ->
			TField (aux t l, ta, f)
	| TRecMake f ->
			let f = List.map (fun (x, y, z) -> (x, y, aux z l)) f in
			TRecMake f
	| TRecSet (o, f) ->
			let f = List.map (fun (x, y, z) -> (x, y, aux z l)) f in
			TRecSet (aux o l, f)
	end
and inline_monads_skel ss l sk =
	let aux' x l = inline_monads_term ss l x in
	let aux x l = inline_monads_skel ss l x in
	begin match sk with
	| LetIn (a, p, s1, s2) ->
			begin match a with
			| None ->
					LetIn (None, p, aux s1 l, aux s2 l)
			| Some (_, Some _, _, _, _) ->
					(* For now, don't unfold let-bindings defined in other files *)
					LetIn (a, p, aux s1 l, aux s2 l)
			| Some (_, None, bind, _ta, _) when List.mem bind l ->
					Format.kasprintf failwith
					"Loop detected in binding operators, including %s" bind
			| Some (_, None, bind, ta_bind, _) -> (* Here's the real work *)
					begin match SMap.find_opt bind ss.ss_terms with
					| None -> raise InternalError
					| Some (_com, _ta, _ty, None) ->
							(* bind is non-specified *)
							LetIn (a, p, aux s1 l, aux s2 l)
					| Some (_com, ta, _ty, Some v) ->
							(* specialize definition of bind*)
							let tae =
								begin try List.combine ta ta_bind
								with _ -> raise InternalError end
							in
							let x, ty_x, f, ty_f, sk =
								begin match specialize_term tae v with
								| TFunc (PVar (x, ty_x), Return (TFunc (PVar (f, ty_f), sk))) ->
										(x, ty_x, f, ty_f, sk)
								| TFunc (PVar (x, ty_x), sk1) ->
										let f = fresh ((<>) x) "f" in
										let ty_f, _ty_sk, _ty_sk1 =
											begin match unalias ss (skeleton_type ss sk1) with
											| Arrow (a, b) -> a, b, Arrow (a, b)
											| _ -> raise InternalError
											end
										in
										let sk = apply ss sk1 [TVar (TVLet (f, ty_f))] in
										(x, ty_x, f, ty_f, sk)
								| sk1 ->
										let x, f = "x", "f" in
										let ty_x, ty_f, _ty_sk, _ty_sk1 =
											begin match unalias ss (term_type ss sk1) with
											| Arrow (a, b) ->
													begin match unalias ss b with
													| Arrow (c, d) -> a, c, d, Arrow (a, Arrow(c, d))
													| _ -> raise InternalError
													end
											| _ -> raise InternalError
											end
										in
										let sk = Apply (sk1,
											[ TVar (TVLet (x, ty_x)); (TVar (TVLet (f, ty_f)))]) in
										(x, ty_x, f, ty_f, sk)
								end
							in
							(* recursive call on s2 *)
							let s2 = inline_monads_skel ss l s2 in
							(* inline binds in sk *)
							let ok y = not (List.mem y
								(all_variables_list_skel s2)) in
							let x_new = fresh ok x in
							let sk = inline_monads_skel ss (bind :: l) sk in
							(* substitute [x] with [x_new] and [f] with [λ p → s2] *)
							let ty_f_in =
								begin match unalias ss ty_f with
								| Arrow (i, _o) -> i
								| _ -> raise InternalError
								end
							in
							let x_new_term = TVar (TVLet (x_new, ty_x)) in
							let sk = skel_map [x, x_new_term] sk in
							let sk =
								begin match p with
								| PVar (x, _ty) ->
										subst_sk ss f (Some x) ty_f_in s2 sk
								| PTuple [] | PWild _ ->
										subst_sk ss f None ty_f_in s2 sk
								| _ ->
										let f_new_term =
												TFunc (p, s2)
										in
										skel_map [f, f_new_term] sk
								end
							in
							LetIn (None, PVar (x_new, ty_x), aux s1 l, sk)
					end
			end
	| Exists (p, ty, s) ->
		Exists (p, ty, aux s l)
	| Apply (t, tl) ->
		let tl = List.map (fun x -> aux' x l) tl in
		Apply (aux' t l, tl)
	| Branching (s, b) ->
		let b = List.map (fun x -> aux x l) b in
		Branching (s, b)
	| Match (t, ty, psl) ->
			Match (aux' t l, ty, List.map (fun (p, s) -> (p, aux s l)) psl)
	| Return t ->
		 Return (aux' t l)
	end

let inline_monads ss =
	let ss_terms =
		SMap.map (fun (com, ta, ty, v) ->
		let v = Option.map (inline_monads_term ss []) v in
		(com, ta, ty, v)
		) ss.ss_terms
	in
	{ ss with ss_terms }





(*****************************************************************************)
(*                       eta expanse specified terms                        *)
(*****************************************************************************)

let rec ident_of_type ty =
	begin match ty with
	| Variable x -> x
	| UserType (_, s, _, _) -> s
	| Arrow _ -> "function"
	| Product l -> String.concat "_" (List.map ident_of_type l)
	end

let ident_of_types tyl taken =
	let rec aux tyl taken =
		(* returns (list of taken, ident_of_types) *)
		begin match tyl with
		| [] -> taken, []
		| a :: q ->
				let taken, q = aux q taken in
				let id_a = fresh (fun x -> not (List.mem x taken)) (ident_of_type a) in
				id_a :: taken, id_a :: q
		end
	in snd (aux tyl taken)

let get_args ss ty =
	(* for [A → B → C → D], we get [C;B;A] *)
	let rec aux ty accu =
		begin match unalias ss ty with
		| Arrow (a, b) ->
				aux b (a :: accu)
		| _ -> accu
		end
	in aux ty []

let rec eta_expanse_skel ss ty sk taken =
	begin match unalias ss ty, sk with
	| Arrow (_a, b), Return (TFunc (p, sk')) ->
			let taken = pattern_variables p @ taken in
			Return (TFunc (p, eta_expanse_skel ss b sk' taken))
	| Arrow _, s ->
			let a_ty = get_args ss ty in
			let a_x = ident_of_types a_ty taken in
			let a_v = List.rev_map2 (fun x ty ->
				TVar (TVLet (x, ty))) a_x a_ty in
			(* fold left because we begin with the start of the list, which is the
			   right-most argument *)
			List.fold_left2 (fun sk x ty ->
				Return (TFunc (PVar (x, ty), sk)))
				(apply ss s a_v) a_x a_ty
	| _ -> (* all expanded *)
			sk
	end

let eta_expanse_term ss ty v =
	begin match unalias ss ty with
	| Arrow _ ->
			begin match eta_expanse_skel ss ty (Return v) [] with
			| Return v -> v
			| _ -> raise InternalError
			end
	| _ -> (* not functional *)
			v
	end

let eta_expanse ss =
	let ss_terms =
		SMap.map (fun (com, ta, ty, v) ->
		let v = Option.map (eta_expanse_term ss ty) v in
		(com, ta, ty, v)
		) ss.ss_terms
	in
	{ ss with ss_terms }



(*****************************************************************************)
(*                           inline specific term                           *)
(*****************************************************************************)



let rec inline_term_aux ss w w_cont v=
let aux x = inline_term_aux ss w w_cont x in
let aux' x = inline_skel_aux ss w w_cont x in
begin match v with
| TVar (TVTerm (_, None,v,_,_)) when v = w -> w_cont
| TVar v -> TVar v
| TConstr (c, ta, t) -> TConstr (c, ta, aux t)
| TTuple t ->
		let t = List.map aux t in
		TTuple t
| TFunc (p, s) ->
		TFunc (p, aux' s)
| TField (t, ta, f) ->
		TField (aux t, ta, f)
| TRecMake f ->
		let f = List.map (fun (x, y, z) -> (x, y, aux z)) f in
		TRecMake f
| TRecSet (o, f) ->
		let f = List.map (fun (x, y, z) -> (x, y, aux z)) f in
		TRecSet (aux o, f)
end
and inline_skel_aux ss w w_cont sk =
let aux' x = inline_term_aux ss w w_cont x in
let aux x = inline_skel_aux ss w w_cont x in
begin match sk with
| LetIn (a, p, s1, s2) ->
	LetIn (a, p, aux s1, aux s2)
| Exists (p, ty, s) ->
	Exists (p, ty, aux s)
| Apply (TVar (TVTerm (_,None,v,_,_)), tl) when v = w ->
	let tl = List.map aux' tl in
	apply ss (Return w_cont) tl
| Apply (t, tl) ->
	let tl = List.map aux' tl in
	Apply (aux' t, tl)
| Branching (s, b) ->
	let b = List.map aux b in
	Branching (s, b)
| Match (t, ty, psl) ->
	let psl = List.map (pair_app id aux) psl in
	Match (aux' t, ty, psl)
| Return t ->
	 Return (aux' t)
end

(* Checks that the term v is not self-recursive *)
let check v name =
	let check_interp =
		{
			var_interp = (fun tv ->
				begin match tv with
				| TVTerm (_, _, w, _, _) when w = name -> false
				| _ -> true
				end) ;
		constr_interp = (fun _c _ta t -> t) ;
		tuple_interp = (fun tl -> List.for_all id tl) ;
		function_interp = (fun _ s -> s) ;
		field_interp = (fun t _ta _f -> t) ;
		record_interp = (fun l ->
			List.for_all (fun (_, _, b) -> b) l) ;
		recset_interp = (fun o l ->
			o && List.for_all (fun (_, _, b) -> b) l) ;
		letin_interp = (fun a _p s1 s2 -> s1 && s2 &&
			begin match a with
			| Some (_, None, w, _, _) when w = name -> false
			| _ -> true
			end) ;
		exists_interp = (fun _p _ty s -> s) ;
		apply_interp = (fun t tl -> t && List.for_all id tl) ;
		merge_interp = (fun _ty sl -> List.for_all id sl) ;
		match_interp = (fun s _ psl -> s && List.for_all snd psl) ;
		return_interp = (fun t -> t)
	}

	in interpret_term check_interp v

let inline_term w ss =
	let w_cont =
		begin match SMap.find_opt w ss.ss_terms with
		| None -> raise InternalError
		| Some (_, _, _, None) -> raise InternalError
		| Some (_, _, _, Some w_cont) ->
				if not (check w_cont w) then
					failwith "Cannot inline a self-recursive term"
				else w_cont
		end
	in
	let ss_terms =
		SMap.mapi (fun name (com, ta, ty, v) ->
			if name = w then (com, ta, ty, v) else
			let v = Option.map (inline_term_aux ss w w_cont) v in
			(com, ta, ty, v)
		) ss.ss_terms
	in
	{ ss with ss_terms }


(*****************************************************************************)
(*                          ignore unused variables                          *)
(*****************************************************************************)

let rec remove_from_pattern x p =
	begin match p with
	| PWild _ -> p
	| PVar (y, ty) when y = x -> PWild ty
	| PVar _ -> p
	| PConstr (c, ty, p) -> PConstr (c, ty, remove_from_pattern x p)
	| PTuple pl -> PTuple (List.map (remove_from_pattern x) pl)
	| PRecord fl ->
			let do_one (f, ta, p) =
				(f, ta, remove_from_pattern x p)
			in
			PRecord (List.map do_one fl)
	end

let remove_unused_pat_sk p sk =
	let vars = pattern_variables p in
	let fv = List.fold_left (fun s (x, _) ->
		SSet.add x s) SSet.empty (free_variables_skel sk) in
	List.fold_left (fun p v ->
		if (SSet.mem v fv) then p
		else remove_from_pattern v p) p vars

let rec remove_unused_in_term v =
	let aux = remove_unused_in_term in
	let aux' = remove_unused_in_skel in
	begin match v with
	| TVar _tv -> v
	| TConstr (c, ta, v) -> TConstr (c, ta, aux v)
	| TTuple vl -> TTuple (List.map aux vl)
	| TFunc (p, sk) ->
			let p = remove_unused_pat_sk p sk in
			TFunc (p, aux' sk)
	| TField (v, ta, f) ->
			TField (aux v, ta, f)
	| TRecMake fl ->
			let do_one (f, ta, v) =
				(f, ta, aux v)
			in
			TRecMake (List.map do_one fl)
	| TRecSet (vo, fl) ->
			let do_one (f, ta, v) =
				(f, ta, aux v)
			in
			TRecSet (aux vo, List.map do_one fl)
	end

and remove_unused_in_skel sk =
	let aux = remove_unused_in_skel in
	let aux' = remove_unused_in_term in
	begin match sk with
	| Branching (ty, sl) -> Branching (ty, List.map aux sl)
	| Match (t, ty, psl) ->
			let do_one (p, s) =
				let p = remove_unused_pat_sk p s in
				(p, aux s)
			in
			Match (aux' t, ty, List.map do_one psl)
	| LetIn (b, p, s1, s2) ->
			let p = remove_unused_pat_sk p s2 in
			LetIn (b, p, aux s1, aux s2)
	| Exists (p, ty, s) ->
			let p = remove_unused_pat_sk p s in
			Exists (p, ty, aux s)
	| Return v -> Return (aux' v)
	| Apply (v, vl) -> Apply (aux' v, List.map aux' vl)
	end

let remove_unused ss =
	let ss_terms = SMap.map (fun (com, ta, ty, t) ->
		(com, ta, ty, Option.map remove_unused_in_term t)) ss.ss_terms
	in
	{ss with ss_terms}


*)










(*
(*********************************************************)
(*                    Module inlining                    *)
(*********************************************************)

module PMap = Map.Make(struct
		type t = Path.Abs.t * String.t
		let compare = Pair.compare Path.Abs.compare String.compare
	end)

type remapping =
	{ constructors : string PMap.t
	; fields : string PMap.t
	; types : string PMap.t
	; terms : string PMap.t
	; binders : string PMap.t
	}
type kind = Constructor | Field | Type | Term | Binder

type 'a rmonad = remapping -> 'a * remapping
let ret (x:'a): 'a rmonad = fun m -> (x, m)
let bind (x:'a rmonad) (f: 'a -> 'b rmonad): 'b rmonad =
	fun m ->
	let (x, m) = x m in
	f x m
let ( let* ) = bind
let traverse_list (xl:'a rmonad list): 'a list rmonad = fun m ->
	List.fold_left_map (fun m x -> x m |> Pair.swap) m xl
	|> Pair.swap
let traverse_map (map:'a rmonad SMap.t): 'a SMap.t rmonad =
	fun m -> SMap.fold (fun x y (map, m) ->
		let y, m = y m in
		(SMap.add x y map, m)) map (SMap.empty, m)
let traverse_pair ((a, b):'a rmonad * 'b rmonad): ('a * 'b) rmonad = fun m ->
	let a, m = a m in
	let b, m = b m in
	((a, b), m)
let traverse_opt (o:'a rmonad option): 'a option rmonad =
	begin match o with
	| None -> ret @@ None
	| Some a ->
			let* a = a in
			ret @@ Some a
	end
(* used for a rmonad list *)
let (let+) x f = bind (traverse_list x) f
(* used for (a rmonad * b rmonad) list : useful with records *)
let (let$) x f = bind (traverse_list (List.map traverse_pair x)) f
(* used for a option rmonad *)
let (let-?) x f = bind (traverse_opt x) f
(* used for maps *)
let (let>) x f = bind (traverse_map x) f

let empty_remapping: remapping =
	{ constructors = PMap.empty ; fields = PMap.empty ; types = PMap.empty ; terms
	= PMap.empty ; binders = PMap.empty }

(* Choice done here :
	- identifiers in the main file in toplevel are replaced by _id
	- identifiers in module a::b are replaced by id__b__a
	- constructors in module a::b are replaced by C__b__a
	- binder @ in module a::b is replaced by @__b__a
	*)
let expand_name (f, c, b) main k
		(name: string ParsedAST.ResolvedAST.with_path) m: string with_path * remapping =
	let new_name k (name:string ParsedAST.ResolvedAST.with_path) m =
		let map, remap =
			begin match k with
			| Constructor -> m.constructors, (fun m constructors -> {m with constructors})
			| Field -> m.fields, (fun m fields -> {m with fields})
			| Type -> m.types, (fun m types -> {m with types})
			| Term -> m.terms, (fun m terms -> {m with terms})
			| Binder -> m.binders, (fun m binders -> {m with binders})
			end
		in
		begin match PMap.find_opt (name.abs, name.obj) map with
		| Some x -> (x, m)
		| None when k = Constructor && c -> name.obj, m
		| None when k = Field && f -> name.obj, m
		| None when k = Binder && b -> name.obj, m
		| None ->
				let rec expand_rel obj (name: string Path.Rel.t): string =
					begin match name with
					| Ident x -> obj ^ "__" ^ x
					| Dot (x, y) -> expand_rel obj y ^ "__" ^ x
					end
				in
				let new_name =
					begin match name.abs with
					| Local -> name.obj (* local name, do nothing *)
					| File (f, Some r) when f = main -> expand_rel name.obj r
					| File (f, None) when f = main -> name.obj
					| File (f, Some r) -> expand_rel name.obj r ^ "__" ^ f
					| File (f, None) -> name.obj ^ "__" ^ f
					end
				in
				let m = remap m (PMap.add (name.abs, name.obj) new_name map) in
				(new_name, m)
		end
	in
	let new_name, m = new_name k name m in
	{abs = File (main, None) ; rel = Ident new_name ; obj = new_name }, m

let rec expand_path_type opt main ty: typ rmonad =
	begin match ty with
	| Variable x -> ret @@ Variable x
	| UserType (ty, ta) ->
			let* ty = expand_name opt main Type ty in
			let+ ta = List.map (expand_path_type opt main) ta in
			ret @@ UserType (ty, ta)
	| Arrow (tyi, tyo) ->
			let* tyi = expand_path_type opt main tyi in
			let* tyo = expand_path_type opt main tyo in
			ret @@ Arrow (tyi, tyo)
	| Product tyl ->
			let+ tyl = List.map (expand_path_type opt main) tyl in
			ret @@ Product tyl
	end

let expand_path_term opt main (t: term): term rmonad =
	let aux_bind b: bind rmonad =
		begin match b with
		| NoBind -> ret @@ NoBind
		| TermBind (k, x, ta, ty) ->
				let* x = expand_name opt main Term x in
				let+ ta = List.map (expand_path_type opt main) ta in
				let* ty = expand_path_type opt main ty in
				ret @@ TermBind (k, x, ta, ty)
		| SymbolBind (sym, k, x, ta, ty) ->
				let* {obj=sym;_} = expand_name opt main Binder {x with obj=sym} in
				let* x = expand_name opt main Term x in
				let+ ta = List.map (expand_path_type opt main) ta in
				let* ty = expand_path_type opt main ty in
				ret @@ SymbolBind (sym, k, x, ta, ty)
		end
	in
	let aux_constr_field k (c, (ty, ta)): _ rmonad =
		let* {obj=ty;_} = expand_name opt main Type {c with obj=ty} in
		let* c = expand_name opt main k c in
		let+ ta = List.map (expand_path_type opt main) ta in
		ret @@ (c, (ty, ta))
	in
	let aux_constr: constructor -> constructor rmonad = aux_constr_field Constructor in
	let aux_field: field -> field rmonad = aux_constr_field Field in
	let rec aux_pattern p: pattern rmonad =
		(fun pdesc ->
			let* pdesc = pdesc in
			let* ptyp = expand_path_type opt main p.ptyp in
			ret @@ {p with pdesc; ptyp})
		begin match p.pdesc with
		| PWild -> ret @@ PWild
		| PVar x -> ret @@ PVar x
		| PConstr (c, p) ->
				let* c = aux_constr c in
				let* p = aux_pattern p in
				ret @@ PConstr (c, p)
		| PTuple pl ->
				let+ pl = List.map aux_pattern pl in
				ret @@ PTuple pl
		| PRecord fpl ->
				let$ fpl = List.map (Pair.map aux_field aux_pattern) fpl in
				ret @@ PRecord fpl
		| POr (p1, p2) ->
				let* p1 = aux_pattern p1 in
				let* p2 = aux_pattern p2 in
				ret @@ POr (p1, p2)
		| PType (p, ty) ->
				let* p = aux_pattern p in
				let* ty = expand_path_type opt main ty in
				ret @@ PType (p, ty)
		end
	in
	let rec aux_term t: term rmonad =
		(fun tdesc ->
			let* tdesc = tdesc in
			let* ttyp = expand_path_type opt main t.ttyp in
			ret @@ {t with tdesc; ttyp})
		begin match t.tdesc with
		| TVar (LetBound _) -> ret @@ t.tdesc
		| TVar (TopLevel (k, n, ta, ty)) ->
				let* n = expand_name opt main Term n in
				let* ty = expand_path_type opt main ty in
				let+ ta = List.map (expand_path_type opt main) ta in
				ret @@ TVar (TopLevel (k, n, ta, ty))
		| TConstr (c, t) ->
				let* c = aux_constr c in
				let* t = aux_term t in
				ret @@ TConstr (c, t)
		| TTuple tl ->
				let+ tl = List.map aux_term tl in
				ret @@ TTuple tl
		| TFunc (p, s) ->
				let* p = aux_pattern p in
				let* s = aux_skel s in
				ret @@ TFunc (p, s)
		| TField (t, f) ->
				let* t = aux_term t in
				let* f = aux_field f in
				ret @@ TField (t, f)
		| TNth (t, ta, n) ->
				let* t = aux_term t in
				let+ ta = List.map (expand_path_type opt main) ta in
				ret @@ TNth (t, ta, n)
		| TRecMake ftl ->
				let$ ftl = List.map (Pair.map aux_field aux_term) ftl in
				ret @@ TRecMake ftl
		| TRecSet (t, ftl) ->
				let* t = aux_term t in
				let$ ftl = List.map (Pair.map aux_field aux_term) ftl in
				ret @@ TRecSet (t, ftl)
		end
	and aux_skel s =
		(fun sdesc ->
			let* sdesc = sdesc in
			let* styp = expand_path_type opt main s.styp in
			ret @@ {s with sdesc; styp})
		begin match s.sdesc with
		| Branching (ty, sl) ->
				let* ty = expand_path_type opt main ty in
				let+ sl = List.map aux_skel sl in
				ret @@ Branching (ty, sl)
		| Match (s, psl) ->
				let* s = aux_skel s in
				let$ psl = List.map (Pair.map aux_pattern aux_skel) psl in
				ret @@ Match (s, psl)
		| LetIn (b, p, s1, s2) ->
				let* b = aux_bind b in
				let* p = aux_pattern p in
				let* s1 = aux_skel s1 in
				let* s2 = aux_skel s2 in
				ret @@ LetIn (b, p, s1, s2)
		| Exists ty ->
				let* ty = expand_path_type opt main ty in
				ret @@ Exists ty
		| Return t ->
				let* t = aux_term t in
				ret @@ Return t
		| Apply (f, x) ->
				let* f = aux_term f in
				let* x = aux_term x in
				ret @@ Apply (f, x)
		end
	in
	aux_term t

(* cs_name and cs_variant_type must be fixed elsewhere *)
let expand_path_csig opt main csig =
	let* cs_input_type = expand_path_type opt main csig.cs_input_type in
	ret @@ {csig with cs_input_type}
(* fs_name and fs_record_type must be fixed elsewhere *)
let expand_path_fsig opt main fsig =
	let* fs_field_type = expand_path_type opt main fsig.fs_field_type in
	ret @@ {fsig with fs_field_type }

let expand_path_ss opt main ss =
	let expand_path_ss_type opt (com, td) =
		begin match td with
		| TDUnspec _ -> ret @@ (com, td)
		| TDVariant (ta, csl) ->
				let+ csl = List.map (expand_path_csig opt main) csl in
				ret @@ (com, TDVariant (ta, csl))
		| TDRecord (ta, fsl) ->
				let+ fsl = List.map (expand_path_fsig opt main) fsl in
				ret @@ (com, TDRecord (ta, fsl))
		| TDAlias (ta, ty) ->
				let* ty = expand_path_type opt main ty in
				ret @@ (com, TDAlias (ta, ty))
		end
	in

	let expand_path_ss_term opt (com, (ta, ty, t)) =
		let* ty = expand_path_type opt main ty in
		let-? t = Option.map (expand_path_term opt main) t in
		ret @@ (com, (ta, ty, t))
	in

	let expand_path_ss_binders opt (k, t) =
		let* t = expand_name opt main Term t in
		ret @@ (k, t)
	in

	let ss_order = ss.ss_order in
	let> ss_types   = SMap.map (expand_path_ss_type    opt) ss.ss_types in
	let> ss_terms   = SMap.map (expand_path_ss_term    opt) ss.ss_terms in
	let> ss_binders = SMap.map (expand_path_ss_binders opt) ss.ss_binders in
	let ss_modules =
		assert(SMap.is_empty ss.ss_modules) ;
		SMap.empty
	in
	ret @@ { ss_order ; ss_types ; ss_terms ; ss_binders ; ss_modules ; ss_opens=[]}

(* take the skeletal semantics with updated names, and collapse it into one
	 module-less skeletal semantics *)
let collapse (f, c, b) deps main ss: skeletal_semantics * remapping =
	let rename k path x m =
		let map, remap =
			begin match k with
			| Constructor -> m.constructors, (fun m constructors -> {m with constructors})
			| Field -> m.fields, (fun m fields -> {m with fields})
			| Type -> m.types, (fun m types -> {m with types})
			| Term -> m.terms, (fun m terms -> {m with terms})
			| Binder -> m.binders, (fun m binders -> {m with binders})
			end
		in
		begin match PMap.find_opt (path, x) map with
		| Some x -> x, m
		| None ->
				let {ParsedAST.obj=newname;_}, m = expand_name (f, c, b) main k {obj=x;abs=path;rel=Ident ""} m in
				newname, (remap m (PMap.add (path, x) newname map))
		end
	in
	let merge_map: type a. a SMap.t -> Path.Absolute.t -> a SMap.t -> kind -> a SMap.t rmonad =
		fun map path map_path k m ->
		List.fold_left (fun (map, m) (x, v) ->
			let x', m = rename k path x m in
			begin match SMap.find_opt x' map with
			| Some _ -> assert false (* TODO: handle this, what to do? *)
			| None -> SMap.add x' v map, m
			end) (map, m) (SMap.bindings map_path)
	in
	(* Different from ss_map, because we need to fix csigs and fsigs *)
	let merge_ss_types map path map_path m =
		let fix_csig cs =
			let* cs_name = rename Constructor path cs.cs_name in
			let* cs_variant_type = rename Type path cs.cs_variant_type in
			ret @@ {cs with cs_name ; cs_variant_type }
		in
		let fix_fsig fs =
			let* fs_name = rename Field path fs.fs_name in
			let* fs_record_type = rename Type path fs.fs_record_type in
			ret @@ {fs with fs_name ; fs_record_type }
		in
		SMap.fold (fun x (com, td) (map, m) ->
			let x', m = rename Type path x m in
			begin match SMap.find_opt x' map with
			| Some _ -> assert false (* TODO: handle this, what to do? *)
			| None ->
					let td, m =
						begin match td with
						| TDVariant (ta, csl) ->
								let csl, m = traverse_list (List.map fix_csig csl) m in
								TDVariant (ta, csl), m
						| TDRecord  (ta, fsl) ->
								let fsl, m = traverse_list (List.map fix_fsig fsl) m in
								TDRecord  (ta, fsl), m
						| TDAlias _ | TDUnspec _ -> td, m
						end
					in
					SMap.add x' (com, td) map, m
			end) map_path (map, m)
	in
	let rec make_ss_order suffix ss =
		let aux suffix (d:ParsedAST.decl_kind) =
			begin match d with
			| Comment _ -> [d]
			| TypeDecl t -> [TypeDecl (t ^ suffix)]
			| TermDecl t -> [TermDecl (t ^ suffix)]
			| BinderDecl t -> [BinderDecl (t ^ suffix)]
			| ModuleDecl m ->
					begin match SMap.find_opt m ss.ss_modules with
					| None -> assert false
					| Some (_, ss_m) ->
							ParsedAST.Comment (mkloc ghost_loc (Format.sprintf " BEGIN MODULE %s " m)) ::
								make_ss_order ("__" ^ m ^ suffix) ss_m @
							[Comment (mkloc ghost_loc (Format.sprintf " END MODULE %s " m))]
					end
			end
		in
		List.concat_map (aux suffix) ss.ss_order
	in
	let rec merge_ss ss path ss_path m =
		let ss_order = [] in (* handled later *)
		let ss_types, m = merge_ss_types ss.ss_types path ss_path.ss_types m in
		let ss_terms, m = merge_map ss.ss_terms path ss_path.ss_terms Term m in
		let ss_binders, m = merge_map ss.ss_binders path ss_path.ss_binders Binder m in
		let ss_modules = SMap.empty in
		let ss =
			{ss_order ; ss_types ; ss_terms ; ss_binders ; ss_modules ; ss_opens=[]}
		in
		SMap.fold (fun x (_, ss_mod) (ss, m) -> merge_ss ss (Path.Abs.append path x) ss_mod m)
		ss_path.ss_modules (ss, m)
	in
	let m = empty_remapping in
	let ss_collapsed, m =
		SMap.fold (fun x (_, ss_mod) (ss, m) -> merge_ss ss (File (main, Some (Ident x))) ss_mod m)
		ss.ss_modules ({ss with ss_modules = SMap.empty}, m)
	in
	let ss_order = make_ss_order "" ss in
	let ss_collapsed = {ss_collapsed with ss_order} in
	let ss_with_deps, m =
		List.fold_left (fun (ss, m) (dep_name, dep_ss) ->
			merge_ss ss (File (dep_name, None)) dep_ss m) (ss_collapsed, m) deps
	in
	let fix_pattern ty_t p =
		let ty_p =
			begin match Skeleton.unalias [] main ss_with_deps ty_t with
			| Arrow (ty_p, _) -> ty_p
			| _ -> assert false
			end
		in
		begin match p.pdesc with
		| PType _ -> p
    | _ -> {p with pdesc = PType (p, ty_p)}
		end
	in
	let rec fix_term t =
		(fun tdesc -> { t with tdesc })
		begin match t.tdesc with
		| TFunc (p, ({sdesc=Return t2;_} as s))  ->
				TFunc (fix_pattern t.ttyp p, {s with sdesc=Return (fix_term t2)})
		| TFunc (p, s) ->
				TFunc (fix_pattern t.ttyp p, s)
		| _ -> t.tdesc
		end
	in
	let ss_terms = SMap.map (fun (c, (ta, ty, t)) ->
		(c, (ta, ty, Option.map fix_term t))) ss_with_deps.ss_terms in
	{ss_with_deps with ss_terms}, m

(** all modules are collapsed, and terms/types/constructors/fields are given new identifiers *)
let flatten_modules opt deps (main, ss) =
	let (f, c, b) =
		( (List.mem "-f" opt),
		  (List.mem "-c" opt),
		  (List.mem "-b" opt))
	in
	let getname f =
		let open Filename in remove_extension @@ basename f
	in
	let main = getname main in
	let deps = List.map (Pair.map_fst getname) deps in
	let collapsed, m = collapse (f, c, b) deps main ss in
	let res, m = expand_path_ss (f, c, b) main collapsed m in
	res, m

*)


(*********************************************************)
(*                    Remove shadowing                   *)
(*********************************************************)

let fresh_name taken x =
  let checksout name = not @@ SSet.mem name taken in
  let rec aux i =
    let name = Format.sprintf "%s_%03d" x i in
    if checksout name then name else aux (i+1)
  in
  if checksout x then x else aux 0


let remove_redef_term =
  let remap map x = SMap.find_opt x map |> Option.value ~default:x in
  (* - taken is the set of variable that are already bound to something
     - map is the map of variable that have been renamed to their new name *)
  (* all the work is done at pattern level.
    for each variable in the pattern:
      - We check if it is in [taken], in which case
        - we rename it to a fresh name
        - we add the renaming to map
      - we add the variable name to taken *)
  let remove_redef_pattern (map, taken) p =
    let vars = Skeleton.pattern_variables p |> smap_domain in
    let (map, taken) = SSet.fold (fun var (map, taken) ->
      if not @@ SSet.mem var taken then
        (map, SSet.add var taken)
      else
        (* var was already taken, find a new name *)
        let newvar = fresh_name (SSet.union vars taken) var in
        let map = SMap.add var newvar map in
        let taken = SSet.add newvar taken in
        (map, taken)) vars (map, taken)
    in
    (* use the new names for variables of p *)
    let rec rebind p =
      {p with pdesc =
        begin match p.pdesc with
        | PVar x -> PVar (remap map x)
        | PWild -> PWild
        | PConstr (c, p) -> PConstr (c, rebind p)
        | PTuple pl -> PTuple (List.map rebind pl)
        | PRecord fpl -> PRecord (List.map (Pair.map_snd rebind) fpl)
        | POr (p1, p2) -> POr (rebind p1, rebind p2)
        | PType (p, ty) -> PType (rebind p, ty)
        end}
    in
    ((map, taken), rebind p)
  in
  let rec remove_redef_term env map taken t =
    (fun tdesc -> { t with tdesc}) @@
    begin match t.tdesc with
    | TVar (TopLevel _) -> t.tdesc
    | TVar (LetBound (x, ty)) ->
        begin match SMap.find_opt x map with
        | Some y -> TVar (LetBound (y, ty))
        | None -> t.tdesc
        end
    | TTuple tl ->
        TTuple (List.map (remove_redef_term env map taken) tl)
    | TFunc (p, sk) ->
        let (map', taken'), p = remove_redef_pattern (map, taken) p in
        let env = SSet.union env (Skeleton.pattern_variables p |> smap_domain) in
        TFunc (p, remove_redef_skel env map' taken' sk)
    | TConstr (c, t) ->
        TConstr (c, remove_redef_term env map taken t)
    | TField (t, f) -> TField (remove_redef_term env map taken t, f)
    | TNth (t, tyl, n) -> TNth (remove_redef_term env map taken t, tyl, n)
    | TRecMake ftl ->
        TRecMake (List.map (Pair.map_snd (remove_redef_term env map taken)) ftl)
    | TRecSet (t, ftl) -> TRecSet (remove_redef_term env map taken t,
        List.map (Pair.map_snd (remove_redef_term env map taken)) ftl)
    end
  and remove_redef_skel env map taken sk =
    (fun sdesc -> { sk with sdesc}) @@
    begin match sk.sdesc with
    | Return t ->
        Return (remove_redef_term env map taken t)
    | Branching (ty, sl) ->
        Branching (ty, List.map (remove_redef_skel env map taken) sl)
    | Match (sm, psl) ->
        Match (remove_redef_skel env map taken sm,
          List.map (fun (p, s) ->
            let (map', taken'), p = remove_redef_pattern (map, taken) p in
            let s = remove_redef_skel env map' taken' s in
            (p, s)) psl)
    | LetIn (b, p, s1, s2) ->
        let b =
          begin match b with
          | TermBind (k, (bpath, None), ta, ty) when SSet.mem bpath env ->
              TermBind (k, (remap map bpath, None), ta, ty)
          | SymbolBind (sym, k, (bpath, None), ta, ty) when SSet.mem bpath env ->
              SymbolBind (sym, k, (remap map bpath, None), ta, ty)
          | _ -> b
          end
        in
        let s1 = remove_redef_skel env map taken s1 in
        let (map, taken), p = remove_redef_pattern (map, taken) p in
        let env2 = SSet.union env (Skeleton.pattern_variables p |> smap_domain) in
        let s2 = remove_redef_skel env2 map taken s2 in
        LetIn (b, p, s1, s2)
    | Exists ty -> Exists ty
    | Apply (f, arg) ->
        Apply (remove_redef_term env map taken f, remove_redef_term env map taken arg)
    end
  in remove_redef_term SSet.empty SMap.empty SSet.empty




(*********************************************************)
(*                    Transform to ANF                   *)
(*********************************************************)

module ANF = struct

  let rec rename_pat (map:string SMap.t) (p:TypedAST.pattern): TypedAST.pattern =
    let remap x =
      match SMap.find_opt x map with
      | Some y -> y
      | None -> assert false (* all variables are remapped *)
    in
    {p with pdesc =
      begin match p.pdesc with
      | PWild -> PWild
      | PVar x -> PVar (remap x)
      | PConstr (c, p) -> PConstr (c, rename_pat map p)
      | PTuple pl -> PTuple (List.map (rename_pat map) pl)
      | POr (p1, p2) -> POr (rename_pat map p1, rename_pat map p2)
      | PRecord fpl -> PRecord (List.map (fun (f, p) ->
          (f, rename_pat map p)) fpl)
      | PType (p, _) -> (rename_pat map p).pdesc
      end}

  let remap map x =
    match SMap.find_opt x map with
    | Some y -> y
    | None -> x
  let remove p map =
    SSet.fold (fun x m -> SMap.remove x m) (smap_domain @@ Skeleton.pattern_variables p) map

  let rec rename_skel (map: string SMap.t) (s:TypedAST.skeleton): TypedAST.skeleton =
    {s with sdesc=
    begin match s.sdesc with
    | Branching (ty, sl) -> Branching (ty, List.map (rename_skel map) sl)
    | LetIn (NoBind, p, b, s1) ->
        LetIn (NoBind, p, rename_skel map b, rename_skel (remove p map) s1)
    | LetIn _ -> failwith "Monadic binders not supported for make ANF"
    | Match (bm, psl) ->
        Match (rename_skel map bm,
        List.map (fun (p,s) -> (p, rename_skel (remove p map) s)) psl)
    | Exists ty -> Exists ty
    | Return t -> Return (rename_term map t)
    | Apply (f, x) -> Apply (rename_term map f, rename_term map x)
    end}
  and rename_term (map: string SMap.t) (t:TypedAST.term): TypedAST.term =
    {t with tdesc=
    begin match t.tdesc with
      | TVar (TopLevel _) -> t.tdesc
      | TVar (LetBound (x, ty)) -> TVar (LetBound (remap map x, ty))
      | TConstr (c, t) -> TConstr (c, rename_term map t)
      | TTuple tl -> TTuple (List.map (rename_term map) tl)
      | TFunc (p, s) -> TFunc (p, rename_skel (remove p map) s)
      | TField (t, f) -> TField (rename_term map t, f)
      | TRecMake ftl -> TRecMake (List.map (Pair.map_snd (rename_term map)) ftl)
      | TRecSet (t, ftl) -> TRecSet (rename_term map t, List.map (Pair.map_snd (rename_term map)) ftl)
      | TNth (t, ty, n) -> TNth (rename_term map t, ty, n)
    end}


  let fix_pat_skel sl (p, s) =
    let pvars = Skeleton.pattern_variables p in
    let taken =
      List.fold_left (fun taken s ->
        Necro.Skeleton.free_variables_skel s |>
        smap_domain |> SSet.union taken) (smap_domain pvars) sl
    in
    let map = SMap.mapi (fun n _ ->
      fresh_name taken n) pvars
    in
    let p' = rename_pat map p in
    let s' = rename_skel map s in
    p', s'


  let new_term t =
    let rec new_term (t:term): term =
      (fun tdesc -> {tloc=t.tloc;ttyp=t.ttyp;tdesc})
      begin match t.tdesc with
      | TVar v -> TVar v
      | TConstr (c, t) -> TConstr (c, new_term t)
      | TTuple tl -> TTuple (List.map new_term tl)
      | TFunc (p, s) -> TFunc (p, new_skel s)
      | TField (t, f) -> TField (new_term t, f)
      | TRecMake ftl -> TRecMake (List.map (Pair.map_snd new_term) ftl)
      | TRecSet (t, ftl) -> TRecSet (new_term t, List.map (Pair.map_snd new_term) ftl)
      | TNth (t, tyl, n) -> TNth (new_term t, tyl, n)
      end
    and new_skel (s:skeleton): skeleton =
      (fun sdesc -> {sloc=s.sloc;styp=s.styp;sdesc})
      begin match s.sdesc with
      | Branching (ty, sl) ->
          Branching (ty, List.map new_skel sl)
      | LetIn (NoBind, p, b, s1) ->
          begin match b.sdesc with
          | Exists _ | Return _ | Apply _ -> (* valid bone *)
              LetIn (NoBind, p, new_skel b, new_skel s1)
          | LetIn (NoBind, p2, b, s2) ->
              (* let p = (let p2 = b in s2) in s1
               ⇒ let p2' = b in let p = s2' in s1 *)
                let (p2', s2') = fix_pat_skel [s2;s1] (p2, s2) in
                let inner_skel_desc = LetIn (NoBind, p, s2', s1) in
                let inner_skel = {sdesc=inner_skel_desc; styp=s1.styp; sloc=s.sloc} in
                let outer_skel_desc = LetIn (NoBind, p2', b, inner_skel) in
                let outer_skel = {sdesc = outer_skel_desc; styp=s1.styp; sloc=s.sloc} in
                (new_skel outer_skel).sdesc
          | LetIn _ -> failwith "Monadic binders not supported for make ANF"
          | Match (sm, psl) ->
              (* let p = (match sm with |pi->si end) in s1
               ⇒ match sm with | pi' -> let p = si' in s1 end *)
                let case (pi, si) =
                  let pi', si' = fix_pat_skel [si; s1] (pi, si) in
                  pi', {s with sdesc=LetIn (NoBind, p, si', s1)}
                in
                let match_desc = Match (sm, List.map case psl) in
                let outer_skel = {sdesc = match_desc; styp=s1.styp; sloc=s.sloc} in
                (new_skel outer_skel).sdesc
          | Branching (_, sl) ->
              (* let p = branch Si end in s1
               ⇒ branch let p = Si in s1 end *)
                let one_branch si = {s with sdesc=LetIn (NoBind, p, si, s1)} in
                let branch = Branching (s.styp, List.map one_branch sl) in
                let outer_skel = {sdesc = branch; styp=s1.styp; sloc=s.sloc} in
                (new_skel outer_skel).sdesc
          end
      | LetIn _ -> failwith "Monadic binders not supported for make ANF"
      | Match (bm, psl) ->
          begin match bm.sdesc with
          | Exists _ | Return _ | Apply _ -> (* valid bone *)
              Match (new_skel bm, List.map (Pair.map_snd new_skel) psl)
          | LetIn (NoBind, p, s1, s2) ->
              (* match (let p = s1 in s2) with | pi -> si end
               ⇒ let p' = s1 in (match s2' with | pj -> sj end) *)
              let p', s2' = fix_pat_skel (s2:: List.map snd psl) (p, s2) in
              let match_desc = Match (s2', psl) in
              let match_ = {sdesc =match_desc; styp=s.styp; sloc=s.sloc} in
              let letin = LetIn (NoBind, p', s1, match_) in
              let outer_skel = {sdesc =letin; styp=s.styp; sloc=s.sloc} in
              (new_skel outer_skel).sdesc
          | LetIn _ -> failwith "Monadic binders not supported for make ANF"
          | Match (sm, qtl) ->
              (* match (match sm with | qj -> tj end) with | pi -> si end
               ⇒ match sm with | qj' -> (match tj' with | pi -> si end) end *)
              let outer_case (qj, tj) =
                let qj', tj' = fix_pat_skel (tj :: List.map snd psl) (qj, tj) in
                let inner_match = Match (tj', psl) in
                let inner_skel = {sdesc=inner_match; styp=s.styp; sloc=s.sloc} in
                qj', inner_skel
              in
              let outer_match = Match (sm, List.map outer_case qtl) in
              let outer_skel = {sdesc =outer_match; styp=s.styp; sloc=s.sloc} in
              (new_skel outer_skel).sdesc
          | Branching (_, sl) ->
              (* match (branch si end) with | pj -> sj end
               ⇒ branch (match si with | pj -> sj end) end *)
              let one_branch si = {s with sdesc=Match (si, psl)} in
              let branch = Branching (s.styp, List.map one_branch sl) in
              let outer_skel = {sdesc = branch; styp=s.styp; sloc=s.sloc} in
              (new_skel outer_skel).sdesc
          end
      | Exists ty -> Exists ty
      | Return t -> Return (new_term t)
      | Apply (f, x) -> Apply (new_term f, new_term x)
      end
    in new_term t
end

module OrPat = struct

  let rec split_pattern p =
    let make pdesc = {p with pdesc} in
    begin match p.pdesc with
    | PVar _ | PWild -> [ p ]
    | POr (p1, p2) ->
        let p1l = split_pattern p1 in
        let p2l = split_pattern p2 in
        p1l @ p2l
    | PConstr (c, p) ->
        let pl = split_pattern p in
        List.map (fun p -> make @@ PConstr (c, p)) pl
    | PType (p, ty) ->
        let pl = split_pattern p in
        List.map (fun p -> make @@ PType (p, ty)) pl
    | PTuple pl ->
        let pll = List.map split_pattern pl in
        let tuples = Util.product pll in
        List.map (fun pl -> make @@ PTuple pl) tuples
    | PRecord fpl ->
        let rec aux (fpl: ('f * pattern) list): ('f * pattern) list list =
          begin match fpl with
          | [] -> [[]]
          | (f, p) :: fpq ->
              let fpq' = aux fpq in
              let pl = split_pattern p in
              List.concat_map (fun p ->
                List.map (fun fpq -> (f, p) :: fpq) fpq') pl
          end
        in
        List.map (fun fpl -> make @@ PRecord fpl) (aux fpl)
    end

  let make_branch loc ty bl =
    match bl with
    | [ b ] -> b
    | _ -> {sloc=loc; styp=ty; sdesc=Branching (ty, bl)}

  let rec fix_term t =
    let make tdesc = {t with tdesc} in
    begin match t.tdesc with
    | TVar _ -> t
    | TTuple tl -> make @@ TTuple (List.map fix_term tl)
    | TConstr (c, t) -> make @@ TConstr (c, fix_term t)
    | TField (t, f) -> make @@ TField (fix_term t, f)
    | TNth (t, tyl, n) -> make @@ TNth (fix_term t, tyl, n)
    | TRecMake ftl -> make @@ TRecMake (List.map (fun (f, t) -> (f, fix_term t)) ftl)
    | TRecSet (t, ftl) -> make @@ TRecSet (fix_term t, List.map (fun (f, t) -> (f, fix_term t)) ftl)
    | TFunc (p, s) ->
        let s = fix_skel s in
        let tmp =
          fresh_name (Skeleton.free_variables_skel s |> smap_domain) "tmp"
        in
        let letin p bind s =
          {sloc=t.tloc; styp=s.styp; sdesc=LetIn (NoBind, p, bind, s)} in
        let pl = split_pattern p in
        let var_tmp = {sloc=p.ploc;styp=p.ptyp;sdesc=
          Return {tloc=p.ploc; ttyp=p.ptyp; tdesc=
            TVar (LetBound (tmp, p.ptyp))
          }} in
        let p_tmp = {p with pdesc=
          PType ({p with pdesc=
            PVar tmp}, p.ptyp)} in
        make @@ TFunc (p_tmp,
          make_branch t.tloc s.styp @@
            List.map (fun p -> letin p (var_tmp) s) pl)
    end
  and fix_skel s =
    match fix_skel_list s with
    | [ s ] -> s
    | sl -> {s with sdesc=Branching (s.styp, sl)}
  and fix_skel_list s =
    let make sdesc = {s with sdesc} in
    begin match s.sdesc with
    | Return t -> [make @@ Return (fix_term t)]
    | Branching (ty, sl) ->
        let sl = List.concat_map fix_skel_list sl in
        [make @@ Branching (ty, sl)]
    | LetIn (binder, p, b, s) ->
        Fun.flip List.concat_map (split_pattern p) @@ fun p ->
        Fun.flip List.concat_map (fix_skel_list b) @@ fun b ->
        Fun.flip List.map (fix_skel_list s) @@ fun s ->
        make @@ LetIn (binder, p, b, s)
    | Match (sm, psl) ->
        Fun.flip List.map (fix_skel_list sm) @@ fun sm ->
        make @@ Match (sm,
          List.concat_map (fun (p, s) ->
            List.map (fun p ->
              (p, fix_skel s)) (split_pattern p)) psl)
    | Exists _ -> [ s ]
    | Apply (f, x) -> [ make @@ Apply (fix_term f, fix_term x) ]
    end
end





module Semantics = struct
  type dependency = (string * skeletal_semantics)

let lift (f: term -> term) =
  fun _deps (_self, sem) ->
    {sem with ss_terms = Util.SMap.map (fun (com, (ta, ty, t)) ->
      (com, (ta, ty, Option.map f t))) sem.ss_terms}
end

module Term = struct
  let remove_redef = remove_redef_term
  let make_anf = ANF.new_term
  let remove_or_patterns = OrPat.fix_term
end
