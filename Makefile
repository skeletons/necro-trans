SHELL=bash
red=`tput setaf 1``tput setab 7`
reset=`tput sgr0`
TESTS_RULES = $(shell ls necro-test/*.sk)
TESTS = $(addprefix test/,$(shell ls test))

NAME := necro-trans
# WARNING : GIT TAG is prefixed with "v"
VERSION := 0.3
TGZ := $(NAME)-v$(VERSION).tar.gz
MD5TXT := $(TGZ).md5sum.txt
OPAM_NAME := necrotrans

# GIT VARIABLES
GITLAB := gitlab.inria.fr
GITPROJECT := skeletons
GIT_REPO := git@$(GITLAB):$(GITPROJECT)/$(NAME).git
GIT_RELEASE := https://$(GITLAB)/$(GITPROJECT)/$(NAME)/-/archive/v$(VERSION)/$(TGZ)

# OPAM VARIABLES
OPAM_REPO_GIT := ../opam-repository
OPAM_FILE := packages/$(OPAM_NAME)/$(OPAM_NAME).$(VERSION)/opam

##########################################################################################
#                       TARGET                                                           #
##########################################################################################

nothing:

# Change version number everywhere where it's specified, and update Makefile
new_version:
	@rm -f $(TGZ) $(MD5TXT)
	@read -p "Please enter new version (current is $(VERSION)): " g;\
	sed -i.bak -r "s#archive/v$(VERSION)#archive/v$$g#" ./necrotrans.opam.template;\
	sed -i.bak -r "s#v$(VERSION).tar.gz#v$$g.tar.gz#" ./necrotrans.opam.template;\
	sed -i.bak -r "s/^VERSION := $(VERSION)/VERSION := $$g/" ./Makefile;\
	sed -i.bak -r "s/^\(version $(VERSION)\)$$/\(version $$g\)/" ./dune-project;\
	git commit ./dune-project ./Makefile ./necrotrans.opam.template -m "Release version $$g";\
	git tag -a v$$g -fm 'OPAM necrotrans package for deployment'
	git push --follow-tags

opam_update :
	@rm -f $(TGZ) $(MD5TXT)
	@wget -q $(GIT_RELEASE)
	@md5sum $(TGZ) > $(MD5TXT)
	@sed -i.bak -r 's|^(\schecksum: "md5\s*=\s*).*"|\1'$$(cat $(MD5TXT) | cut -d" " -f 1)'"|' necrotrans.opam.template
	@dune build
	@mkdir -p $(OPAM_REPO_GIT)/packages/$(OPAM_NAME)/$(OPAM_NAME).$(VERSION)/
	@cp necrotrans.opam $(OPAM_REPO_GIT)/$(OPAM_FILE)

release:
	rm -f $(TGZ) $(MD5TXT)
	@make new_version
	@make opam_update
	cd ../opam-repository; \
		git add packages/necrotrans && git commit -am "Push new Necro Trans version" && git push

rerelease:
	@git tag -f v$(VERSION)
	@git push --tags --force
	@make opam_update
	cd ../opam-repository; \
		git add packages/necrotrans && git commit -am "Upstream change for Necro Trans" && git push

.PHONY: opam_update new_version release nothing
