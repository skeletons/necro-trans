(**************************************************************************)
(*                                                                        *)
(*                           Necro Transformers                           *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

open Necro
open TypedAST
open Necrotrans.Transformation

(** * Auxiliary functions **)
let give_version () =
	Printf.printf "Necro Library, version: %s\n"
	(match Build_info.V1.version () with
	 | None -> "n/a"
	 | Some v -> Build_info.V1.Version.to_string v);
	 exit 0


(** Wrap a function call to make the whole program fail with the right error code if an exception happens. **)
let wrap f x =
	let () = Printexc.record_backtrace true in
	try f x
	with e ->
		let stack = Printexc.get_backtrace () in
		let msg = Printexc.to_string e in
		Printf.eprintf "Fatal error: %s%s\n" msg stack ;
		exit 1
let wrap2 f x y = wrap (wrap f x) y


(* The set of existing skeletal transformations *)
let term_transformers:
	(string * (term -> term)  * string) list =
	("noredef", Term.remove_redef,
	"Rename inner variables of term to prevent shadowing") ::
	("makeanf", Term.make_anf,
	"Push left-nested letins/branches/matches up to make the term ANF") ::
	("removeor", Term.remove_or_patterns,
	"Remove or-patterns from semantics") ::
	[]

let sem_transformers:
	(string *
	( (string * skeletal_semantics) list ->
	  (string * skeletal_semantics) ->
			skeletal_semantics) * string) list =
  []

let lift (f: term -> term) =
  fun _deps (_self, sem) ->
    {sem with ss_terms = Util.SMap.map (fun (com, (ta, ty, t)) ->
      (com, (ta, ty, Option.map f t))) sem.ss_terms}

let all_transformers =
  List.map (fun (name, f, desc) -> (name, lift f, desc)) term_transformers
  @ sem_transformers

(* Print usage in stderr and exit *)
let usage () =
	Printf.eprintf
	("Usage: %s [TRANSFORMER] [OPTION]... [FILE]...\n\n\
	Transforms the last given file using the given transformer. For every file, \
	the dependencies must precede in the list except if the -d option \
	is given.\n\n\
	Options:\n\
	\t-v / --version\tPrint the version and exit.\n\
	\t-d / --find-deps\tOnly one file must be given. The dependencies are then \
	looked for in the cwd, and in the path of the given file.\n\
	\t-o [FILE]/ --output [FILE]\tOutputs the result in given file. If this \
	option is not provided, the result is printed in stdout.\n\n\
	Transformers:\n\
	%s\n")
	Sys.argv.(0) (List.map (fun (name, _, action) ->
		"\t" ^ name ^ ": " ^ action ^ "\n") all_transformers |> String.concat "")
  ;
	exit 1

type minus =
| Depend
| Output of string

let parse l =
	let rec parse accu_files accu_options l =
		begin match l with
		| [] ->
				(accu_files, accu_options)
		| "-d" :: q | "--find-deps" :: q ->
				parse accu_files (Depend :: accu_options) q
		| "-o" :: [] | "--output" :: [] -> usage ()
		| "-o" :: f :: q | "--output" :: f :: q ->
				parse accu_files (Output f :: accu_options) q
		| a :: q ->
				parse (a :: accu_files) accu_options q
		end
	in parse [] [] l


(** MAIN FUNCTION **)
let () =
	let compiler comp =
		begin match List.find_opt (fun (n, _, _) -> n = comp) all_transformers with
		| None ->
				let () = Printf.eprintf "%s is not a valid transformer\n\n" comp in
				usage ()
		| Some (_, c, _) -> c
		end
	in
	let comp, (files, opt) =
		begin match Array.to_list Sys.argv with
		| [] -> assert false
		| _ :: "-v" :: _ | _ :: "--version" :: _ -> give_version ()
		| _ :: c :: l -> compiler c, parse l
		| _ :: [] -> usage ()
		end
	in
	let f, d =
		begin match files with
		| [] -> usage ()
		| a :: [] when List.mem Depend opt -> a, find_deps a
		| _ :: _ when List.mem Depend opt -> usage ()
		| a :: q -> a, List.rev q
		end
	in
	let output_file =
		let l = List.filter_map (function | Output sk -> Some sk | _ -> None) opt in
		begin match l with
		| [] -> None
		| a :: q -> if List.for_all ((=) a) q then Some a else usage ()
		end
	in
	let sem = parse_and_type ~warn:[] d f in
	let deps = List.combine d @@ parse_and_type_list ~warn:[] d in
	let result =
		wrap Printer.string_of_ss (wrap2 comp deps (f, sem)) in
	Util.print_in_file result output_file

