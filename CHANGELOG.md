# Version 0.3
- Update to Necro Lib 0.16
- Unplug `flatten_modules`

# Version 0.2.1
- Add `remove_or_patterns` transformer

# Version 0.2
- Update to NecroLib 0.15.0

# Version 0.1.4
- Define transformer `makeanf` to remove nested let-ins/matches/branches

# Version 0.1.3
- Define transformer `remove_redef` to remove shadowing

# Version 0.1.2
- Update to Necro Lib 0.14.7

# Version 0.1.1
- Add options `-f`, `-c`, and `-b` to `flatten_modules` transformer, in order to
  not rename respectively fields, constructors and binders when flattening. It
  will be useful for NecroML

# Version 0.1
- Add `flatten_modules` transformer

# Version 0.0
- Initialize by taking it out of Necro Lib (version 0.11.2.2)
- Empty it
